<?php

namespace Volt;

use Volt\Mapper\LanguageMapper;

class Language
{
    /**
     * @var \Volt\Mapper\LanguageMapper
     */
    private static $mapper;

    /**
     * @var array
     */
    private static $languages;

    protected function __construct()
    {
    }

    protected function __clone()
    {
    }

    /**
     * @return LanguageMapper
     */
    protected static function getMapper()
    {
        if (null === static::$mapper) {
            static::$mapper = new LanguageMapper();
        }
        return static::$mapper;
    }

    /**
     * @param $code
     *
     * @return Entity\Language
     */
    public static function getByCode($code)
    {
        return static::getMapper()->findByCode($code);
    }

    /**
     * @return mixed
     */
    public static function getLanguagesAsArray()
    {
        $languages = static::getLanguages();
        $data = [];

        /** @var Entity\Language $language */
        foreach ($languages as $key => $language) {
            $data[$language->getCode()] = $language->getArrayCopy();
        }
        return $data;
    }

    /**
     * Get Languages
     *
     * @return mixed
     */
    public static function getLanguages()
    {
        if (null === static::$languages) {
            static::$languages = static::getMapper()->findAll();
        }
        return static::$languages;
    }
}