<?php

namespace Volt;

/**
 * Class VoltResource
 * @package Volt
 */
class VoltResource
{
    /**
     * @return \Doctrine\ORM\EntityManager
     */
    public static function getEntityManager()
    {
        $container = \Zend_Controller_Front::getInstance()->getParam('ServiceContainer');
        return $container->get('vein.entity_manager');
    }
}