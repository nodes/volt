<?php

namespace Volt;

use Volt\Mapper\CountryMapper;

/**
 * Class Country
 *
 * @package Volt
 */
class Country
{
    /**
     * @var \Volt\Mapper\CountryMapper
     */
    private static $mapper;

    /**
     * @var array
     */
    private static $countries;

    protected function __construct()
    {
    }

    protected function __clone()
    {
    }

    /**
     * @return CountryMapper
     */
    protected static function getMapper()
    {
        if (null === static::$mapper) {
            static::$mapper = new CountryMapper();
        }
        return static::$mapper;
    }

    /**
     * @param $code
     *
     * @return Entity\Country
     */
    public static function getByCode($code)
    {
        return static::getMapper()->findByCode($code);
    }

    /**
     * @return mixed
     */
    public static function getNationalitiesAsArray()
    {
        $countries = static::getCountriesAsArray();
        $data = [];

        /** @var Entity\Country $country */
        foreach ($countries as $key => $country) {
            if (empty($country['nationality'])) {
                continue;
            }
            $data[$country['code']] = $country['nationality'];
        }
        return $data;
    }

    /**
     * @return mixed
     */
    public static function getCountriesAsArray()
    {
        $countries = static::getCountries();
        $data = [];

        /** @var Entity\Country $country */
        foreach ($countries as $key => $country) {
            $data[$country->getCode()] = $country->getArrayCopy();
        }
        return $data;
    }

    /**
     * Get Countries
     *
     * @return mixed
     */
    public static function getCountries()
    {
        if (null === static::$countries) {
            static::$countries = static::getMapper()->findAll();
        }
        return static::$countries;
    }
}