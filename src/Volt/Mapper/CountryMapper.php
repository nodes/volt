<?php

namespace Volt\Mapper;

use Doctrine\ORM\EntityManager;
use Volt\Entity\Country;

/**
 * Class CountryMapper
 * @package Volt\Mapper
 */
class CountryMapper
{
    /**
     * Entity Class
     */
    const ENTITY_CLASS = 'Volt\Entity\Country';

    /**
     * @return EntityManager
     */
    public function getEntityManager()
    {
        return \Volt\VoltResource::getEntityManager();
    }

    /**
     * @param $entity
     *
     * @return \Volt\Entity\Country
     */
    public function create(Country $entity)
    {
        return $this->persist($entity);
    }

    /**
     * @param $id
     *
     * @return \Volt\Entity\Country
     */
    public function findByCode($id)
    {
        return $this->getRepository()->findOneBy(['code' => $id]);
    }

    /**
     * Find By
     *
     * @param $key
     * @param $value
     * @return \Volt\Entity\Country
     */
    public function findBy($key, $value)
    {
        return $this->getRepository()->findOneBy(array($key => $value));
    }

    /**
     * Find All
     *
     * @return array
     */
    public function findAll()
    {
        return $this->getRepository()->findAll();
    }

    /**
     * @param Country $entity
     * @return Country
     */
    public function update(Country $entity)
    {
        return $this->persist($entity);
    }

    /**
     * @param Country $entity
     */
    public function delete(Country $entity)
    {
        $this->getEntityManager()->remove($entity);
        $this->getEntityManager()->flush();
    }

    /**
     * Get Repository
     *
     * @return \Doctrine\ORM\EntityRepository
     */
    public function getRepository()
    {
        return $this->getEntityManager()->getRepository('Volt\Entity\Country');
    }

    /**
     * @param Country $entity
     * @return Country
     */
    private function persist(Country $entity)
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}