<?php

namespace Volt\Mapper;

use Doctrine\ORM\EntityManager;
use Volt\Entity\Language;

/**
 * Class LanguageMapper
 * @package Volt\Mapper
 */
class LanguageMapper
{
    /**
     * @return string
     */
    function getEntityClass()
    {
        return 'Volt\Entity\Language';
    }

    /**
     * @return EntityManager
     */
    public function getEntityManager()
    {
        return \Volt\VoltResource::getEntityManager();
    }

    /**
     * @param $entity
     *
     * @return Language
     */
    public function create(Language $entity)
    {
        return $this->persist($entity);
    }

    /**
     * @param $id
     *
     * @return Language
     */
    public function findByCode($id)
    {
        return $this->getRepository()->findOneBy(['code' => $id]);
    }

    /**
     * @param $key
     * @param $value
     * @return Language
     */
    public function findBy($key, $value)
    {
        return $this->getRepository()->findOneBy(array($key => $value));
    }

    /**
     * @return array
     */
    public function findAll()
    {
        return $this->getRepository()->findAll();
    }

    /**
     * Update Language
     *
     * @param Language $entity
     * @return Language
     */
    public function update(Language $entity)
    {
        return $this->persist($entity);
    }

    /**
     * @param $entity
     */
    public function delete(Language $entity)
    {
        $this->getEntityManager()->remove($entity);
        $this->getEntityManager()->flush();
    }

    /**
     * @param Language $entity
     * @return Language
     */
    private function persist(Language $entity)
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    /**
     * Get Repository
     *
     * @return \Doctrine\ORM\EntityRepository
     */
    public function getRepository()
    {
        return $this->getEntityManager()->getRepository('Volt\Entity\Language');
    }
}