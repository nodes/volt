<?php

namespace Volt\Hydrator\Strategy;

use DateTime;
use Zend\Stdlib\Hydrator\Strategy\DefaultStrategy;

/**
 * Class DateTimeStrategy
 *
 * @package Volt\Hydrator\Strategy
 */
class DateTimeStrategy extends DefaultStrategy
{
    /**
     * @var null|string
     */
    private $inputFormat;

    /**
     * @var null|string
     */
    private $outputFormat;

    /**
     * @param string $inputFormat
     */
    public function setInputFormat($inputFormat)
    {
        $this->inputFormat = $inputFormat;
    }

    /**
     * @param string $outputFormat
     */
    public function setOutputFormat($outputFormat)
    {
        $this->outputFormat = $outputFormat;
    }

    /**
     * @param string $outputFormat
     * @param string $inputFormat
     */
    public function __construct($outputFormat = null, $inputFormat = null)
    {
        $this->outputFormat = ($outputFormat === null) ? DateTime::ISO8601 : $outputFormat;
        $this->inputFormat = ($inputFormat === null) ? DateTime::ISO8601 : $inputFormat;
    }

    /**
     * {@inheritdoc}
     *
     * Convert a string value into a DateTime object
     */
    public function hydrate($value)
    {
        if (empty($value)) {
            return null;
        }

        if (is_string($value)) {
            $value = DateTime::createFromFormat($this->inputFormat, $value);
        }
        return $value;
    }

    /**
     * {@inheritdoc}
     *
     * Convert a DateTime into string value
     */
    public function extract($value)
    {
        if ($value instanceof DateTime) {
            $value = $value->format($this->outputFormat);
        }
        return $value;
    }
}