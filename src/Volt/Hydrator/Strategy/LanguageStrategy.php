<?php

namespace Volt\Hydrator\Strategy;

use Volt\Entity\Language;
use Zend\Stdlib\Hydrator\Strategy\DefaultStrategy;

class LanguageStrategy extends DefaultStrategy
{
    /**
     * {@inheritdoc}
     *
     * Convert a string value into a Language object
     */
    public function hydrate($value)
    {
        if (empty($value)) {
            return null;
        }

        if (is_string($value) && strlen($value) == 2) {
            $em = \Volt\VoltResource::getEntityManager();
            $value = $em->getRepository('Volt\Entity\Language')->findOneBy(['code' => $value]);
        }
        return $value;
    }

    /**
     * {@inheritdoc}
     *
     * Convert a Language object into array value
     */
    public function extract($value)
    {
        /** @var Language $value */
        if ($value instanceof Language) {
            $value = [
                'code' => $value->getCode(),
                'name' => $value->getName()
            ];
        }
        return $value;
    }
}