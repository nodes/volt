<?php

namespace Volt\Hydrator\Strategy;

use Doctrine\Common\Collections\Collection;
use Zend\Stdlib\Hydrator\Strategy\DefaultStrategy;
use Zend\Stdlib\Hydrator\Strategy\StrategyInterface;

/**
 * Class DoctrineCollectionStrategy
 * @package Volt\Hydrator\Strategy
 */
class DoctrineCollectionStrategy extends DefaultStrategy
{
    /**
     * @var StrategyInterface
     */
    private $hydrator;

    /**
     * @param StrategyInterface $hydrator
     */
    public function __construct(StrategyInterface $hydrator)
    {
        $this->hydrator = $hydrator;
    }

    /**
     * @param mixed $object
     * @return array|mixed
     * @throws \Exception
     */
    public function extract($object)
    {
        if (!$object instanceof Collection) {
            throw new \Exception('Unsupported object');
        }

        $data = [];
        foreach ($object as $key => $value) {
            $data[$key] = $this->hydrator->extract($value);
        }
        return $data;
    }
}