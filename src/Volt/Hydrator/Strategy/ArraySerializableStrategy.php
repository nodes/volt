<?php

namespace Volt\Hydrator\Strategy;

use Zend\Stdlib\ArraySerializableInterface;
use Zend\Stdlib\Hydrator\Strategy\DefaultStrategy;

/**
 * Class ArraySerializableStrategy
 * @package Volt\Hydrator\Strategy
 */
class ArraySerializableStrategy extends DefaultStrategy
{
    /**
     * {@inheritdoc}
     *
     * Convert a string value into a ArraySerializable object
     */
    public function hydrate($value)
    {
        return $value;
    }

    /**
     * {@inheritdoc}
     *
     * Convert a ArraySerializable into string value
     */
    public function extract($value)
    {
        if ($value instanceof ArraySerializableInterface) {
            $value = $value->getArrayCopy();
        }
        return $value;
    }
}