<?php

namespace Volt\Hydrator\Strategy;

use Volt\Entity\Country;
use Zend\Stdlib\Hydrator\Strategy\DefaultStrategy;

/**
 * Class CountryStrategy
 * @package Volt\Hydrator\Strategy
 */
class CountryStrategy extends DefaultStrategy
{
    /**
     * {@inheritdoc}
     *
     * Convert a string value into a Country object
     */
    public function hydrate($value)
    {
        if (is_string($value) && strlen($value) == 2) {
            $em = \Volt\VoltResource::getEntityManager();
            $value = $em->getRepository('Volt\Entity\Country')->findOneBy(
                ['code' => strtoupper($value)]
            );
        }
        if (empty($value)) {
            $value = null;
        }
        return $value;
    }

    /**
     * {@inheritdoc}
     *
     * Convert a DateTime into string value
     */
    public function extract($value)
    {
        if ($value instanceof Country) {
            $value = [
                'code' => $value->getCode(),
                'country' => $value->getCountry(),
                'nationality' => $value->getNationality(),
            ];
        }
        return $value;
    }
}